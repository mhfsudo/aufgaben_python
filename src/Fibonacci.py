#import fast timer
from timeit import default_timer as timer
#import sys
#sys.setrecursionlimit(10000

# class to calculate the fibonacci numbers
class Fibonacci:

    _fibArray = [0, 1]
    _usedTime = 0

    # Get the n-the fibonaccinumber iterativ
    def GetNTheFibonacciNumberIterative(self, n):
        start = timer()
        a = 0
        b = 1
        # Get only values greater than 0
        if n < 0:
            print("Incorrect input")

        while n - 2:
            c = a + b
            a, b = b, c
            n = n - 1

        stop = timer()
        self.calcTime(start,stop)
        return c

    def calcTime(self, startTime, stopTime):
        self._usedTime = (stopTime-startTime)

    def GetUsedTimeToCalculateNtheNumber(self):
        return self._usedTime

    # private get the n-the fibonaccinumber recursive
    def _Fibonaccirecursive(self, n):
        retVal = 0
        # Get only values greater than 0
        if n < 0:
            print("Incorrect input")
        # First Fibonacci number is 0
        elif n == 1:
            retVal = 0
        # Second Fibonacci number is 1
        elif n == 2:
            retVal = 1
        else:
            retVal = self.GetNTheFibonacciNumberRecursive(n - 1) + self.GetNTheFibonacciNumberRecursive(n - 2)
        return retVal

    def GetNTheFibonacciNumberRecursive(self, n):
        start = timer()
        result = self._Fibonaccirecursive(n)
        stop = timer()
        fib.calcTime(start,stop)
        return result

# create instance of class
fib = Fibonacci()

#set the n-the fibonaccinumber
number = 4

#At position 10 is Fibonacci number 100
print("\n")
print("Recursive calculation of the Fibonacci Number")
print(f"At position {number} is Fibonacci number: {fib.GetNTheFibonacciNumberRecursive(number)}")
print(f"Process time: {fib.GetUsedTimeToCalculateNtheNumber()*1000}ms")

print("\n")
print("Iterative calculation of the Fibonacci Number:")
print(f"At position {number} is Fibonacci number: {fib.GetNTheFibonacciNumberIterative(number)}")
print(f"Process time: {fib.GetUsedTimeToCalculateNtheNumber()*1000}ms")

print("\n")
print("end")
print("\n")

# see how to use time
# https://stackoverflow.com/questions/7370801/measure-time-elapsed-in-python
# https://www.geeksforgeeks.org/private-methods-in-python/
# https://stackoverflow.com/questions/18172257/efficient-calculation-of-fibonacci-series
